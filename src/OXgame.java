import java.util.Scanner;

public class OXgame {
	
	public static void main(String[] args) {
		char turn = 'O';
		showWelcome();
		
		char[][] table = new char[][]{{'-','-','-'},{'-','-','-'},{'-','-','-'}};
		for(;;) {
			showTable(table);
			if(checkWin(table,turn)){
				break;
			}else if(checkDraw(table)) {
				break;
			}
			turn = showTurn(turn);
			System.out.println("It's "+turn+" turn");
			inputRowCol(table, turn);
		}
		showBye();
	}
	
	public static void showWelcome() {
		System.out.println("!! Welcome to OX Game !!");
	}
	
	public static void showTable(char[][] table) {
		for(int i=0;i<3;i++) {
			for(int j=0;j<3;j++) {
				System.out.print(table[i][j]+" ");
			}
			System.out.println();
		}
	}
	public static char showTurn(char turn) {
		if(turn=='X') {
			turn='O';
		}
		else {
			turn='X';
		}
		return turn;
	}
	
	public static void inputRowCol(char[][] table,char turn) {
		Scanner kb = new Scanner(System.in);
		System.out.print("Please input row,column : ");
		int row = kb.nextInt();
		int column = kb.nextInt();
		for(;;) {
			if(table[row-1][column-1]=='-') {
				table[row-1][column-1] = turn;
				break;
			}
			else {
				System.out.print("Please input again : ");
				row = kb.nextInt();
				column = kb.nextInt();
			}
		}
	}
	
	public static boolean checkRow(char[][] table) {
		if(table[0][0] == table[0][1] && table[0][1] == table[0][2] && table[0][0] != '-') {
			return true;
		}else if(table[1][0] == table[1][1] && table[1][1] == table[1][2] && table[1][0] != '-') {
			return true;
		}else if(table[2][0] == table[2][1] && table[2][1] == table[2][2] && table[2][0] != '-') {
			return true;
		}else {
			return false;
		}
	}
	
	public static boolean checkCol(char[][] table) {
		if(table[0][0] == table[1][0] && table[1][0] == table[2][0] && table[0][0] != '-') {
			return true;
		}else if(table[0][1] == table[1][1] && table[1][1] == table[2][1] && table[0][1] != '-') {
			return true;
		}else if(table[0][2] == table[1][2] && table[1][2] == table[2][2] && table[0][2] != '-'){
			return true;
		}else {
			return false;	
		}
	}
	
	public static boolean checkDiag(char[][] table) {
		if(table[0][0] == table[1][1] && table[1][1] == table[2][2] && table[0][0] != '-') {
			return true;
		}else if(table[2][0] == table[1][1] && table[1][1] == table[0][2] && table[2][0] != '-') {
			return true;
		}else {
			return false;
		}
	}
	
	public static boolean checkDraw(char[][] table) {
		int n=0;
		for(int i=0;i<3;i++) {
			for(int j=0;j<3;j++) {
				if(table[i][j] != '-')
					n++;
			}
		}
		if(n==9) {
			System.out.println("!!! DRAW !!!");
			return true;
		}else {
			return false;
		}
	}
	
	public static boolean checkWin(char[][] table,char turn) {
		if(checkRow(table)) {
			System.out.println("!! "+turn+" is a Winner !!");
			return true;
		}else if(checkCol(table)) {
			System.out.println("!! "+turn+" is a Winner !!");
			return true;
		}else if(checkDiag(table)) {
			System.out.println("!! "+turn+" is a Winner !!");
			return true;
		}else {
			return false;
		}
	}
	
	public static void showBye() {
		System.out.println("!! End Game, Bye bye!!");
	}
}
